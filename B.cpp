#include <iostream>
using namespace std;

class B {
    private:
        int B1;
        float B2;
    public:
        void setB1B2(int b, float bb) {
            B1 = b;
            B2 = bb;
        };
        int getB1() {
            return B1;
        };
        float getB2() {
            return B2;
        };
        void MB1() {
            cout << "MB1" << endl;
        };
        void MB2() {
            cout << "MB2" << endl;
        };
};

int main() {
    B classeB;
    classeB.setB1B2(10, 5.1);
    int B1 = classeB.getB1();
    float B2 = classeB.getB2();
    cout << B1 << " " << B2 << endl;
    classeB.MB1();
    classeB.MB2();
    return 0;
};