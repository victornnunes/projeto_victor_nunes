#include <iostream>
using namespace std;

class C {
    private:
        int C1;
        float C2;
    public:
        void setC1C2(int c, float cc) {
            C1 = c;
            C2 = cc;
        };
        int getC1() {
            return C1;
        };
        float getC2() {
            return C2;
        };
        void MC1() {
            cout << "MC1" << endl;
        };
        void MC2() {
            cout << "MC2" << endl;
        };
};

int main() {
    C classeC;
    classeC.setC1C2(10, 5.1);
    int C1 = classeC.getC1();
    float C2 = classeC.getC2();
    cout << C1 << " " << C2 << endl;
    classeC.MC1();
    classeC.MC2();
    return 0;
};