#include <iostream>
using namespace std;

class A {
    private:
        int A1;
        float A2;
    public:
        void setA1A2(int a, float aa) {
            A1 = a;
            A2 = aa;
        };
        int getA1() {
            return A1;
        };
        float getA2() {
            return A2;
        };
        void MA1() {
            cout << "MA1" << endl;
        };
        void MA2() {
            cout << "MA2" << endl;
        };
        void MA3() {
            cout << "Alteração a classe A partir do clone" << endl;
        };
};

int main() {
    A classeA;
    classeA.setA1A2(10, 5.1);
    int A1 = classeA.getA1();
    float A2 = classeA.getA2();
    cout << A1 << " " << A2 << endl;
    classeA.MA1();
    classeA.MA2();
    classeA.MA3();
    return 0;
};